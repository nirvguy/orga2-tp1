
; PALABRA
	global palabraLongitud
	global palabraMenor
	global palabraFormatear
	global palabraImprimir
	global palabraCopiar
	
; LISTA y NODO
	global nodoCrear
	global nodoBorrar
	global oracionCrear
	global oracionBorrar
	global oracionImprimir

; AVANZADAS
	global longitudMedia
	global insertarOrdenado
	global filtrarPalabra
	global descifrarMensajeDiabolico

; YA IMPLEMENTADAS EN C
	extern palabraIgual
	extern insertarAtras

; /** DEFINES **/    >> SE RECOMIENDA COMPLETAR LOS DEFINES CON LOS VALORES CORRECTOS
	%define NULL 		0
	%define TRUE 		1
	%define FALSE 		0

	%define LISTA_SIZE 	    	 8
	%define OFFSET_PRIMERO 		 0

	%define NODO_SIZE     		 16
	%define OFFSET_SIGUIENTE   	 0
	%define OFFSET_PALABRA 		 8


section .rodata


section .data
	FORMATO: DB '%s',10, 0
	MODO_ARCHIVO: DB 'a',0
	SIN_MENSAJE_DIABOLICO: DB '<sinMensajeDiabolico>',0
	ORACION_VACIA: DB '<oracionVacia>', 0

section .text

extern malloc
extern free
extern fopen
extern fclose
extern fputs
extern fprintf

;/** FUNCIONES DE PALABRAS **/

;-----------------------------------------------------------

	; unsigned char palabraLongitud( char *p );
	palabraLongitud:
		push rbp
		mov rbp, rsp
		
		;rdi = indice para el reccorrido del string p
		;rax = contador
		xor rax, rax
		.ciclo:
			cmp byte [rdi], 0
			je .finCiclo
			inc rax
			inc rdi
			jmp .ciclo
		.finCiclo:
		
		pop rbp
		ret

	; bool palabraMenor( char *p1, char *p2 );
	palabraMenor:
		push rbp
		mov rbp, rsp
		
		;rdi = indice para el reccorrido del string p1
		;rsi = indice para el reccorrido del string p2
		
		; avanzar p1,p2 mientras *p1 y *p2 no sean
		; el caracter nulo y ambos sean iguales
		.ciclo:
			cmp byte [rdi], 0
			je .finCiclo
			cmp byte [rsi], 0
			je .finCiclo
			mov al, byte [rsi]
			cmp byte [rdi], al
			jne .finCiclo
			inc rdi
			inc rsi
			jmp .ciclo
		.finCiclo:
		; cuando se llega al final del ciclo
		; hay 3 casos 
		; caso1) *p1 llega a 0 antes que *p2
		; *p1 = 0, p2 (mirando a *p1 y *p2 como unsigned chars) > 0 -> TRUE
		; Ej:
		; ab y aba
		; caso 2) *p2 llega a 0 antes que *p1
		; *p1 > 0, p2 > 0 -> FALSE
		; Ej:
		; aba y ab
		; caso 3) ambos no llegan a 0 y 
		; siendo ninguno prefijo del otro
		; entonces se compara por el caracter con menor ascii
		; Todos estos casos se contemplan con *p1 <= *p2
		; luego del fin del ciclo
		mov al, byte [rdi]
		sub al, byte [rsi]
		jae .false
		
		.true:
		mov rax, TRUE
		jmp .end
		
		.false:
		mov rax, FALSE
		jmp .end
		
		.end:
		pop rbp
		ret

	; void palabraFormatear( char *p, void (*funcModificarString)(char*) );
	palabraFormatear:
		push rbp
		mov rbp, rsp
		
		call rsi
		
		pop rbp
		ret

	; void palabraImprimir( char *p, FILE *file );
	palabraImprimir:
		push rbp
		mov rbp, rsp
		
		mov rdx, rdi
		mov rdi, rsi
		mov rsi, FORMATO
		call fprintf
		
		pop rbp
		ret

	; char *palabraCopiar( char *p );
	palabraCopiar:
		push rbp
		mov rbp, rsp
		push rbx
		push r12
		
		;rbx = puntero al string p
		;r12 = longitud del string + 1
		;rcx = numero de caracter que se esta copiando
		
		mov rbx, rdi
		call palabraLongitud
		inc rax
		mov r12, rax
		mov rdi, rax
		call malloc
		mov rcx, r12
		
		.cicloCopiar:
			mov r10b, byte [rbx+rcx-1]
			mov byte [rax+rcx-1], r10b
			loop .cicloCopiar
		
		pop r12
		pop rbx
		pop rbp
		ret


;/** FUNCIONES DE LISTA Y NODO **/
;-----------------------------------------------------------

	; nodo *nodoCrear( char *palabra );
	nodoCrear:
		push rbp
		mov rbp, rsp
		sub rsp, 8
		push rbx
		
		mov rbx, rdi
		mov rdi, NODO_SIZE
		call malloc
		mov qword [rax+OFFSET_SIGUIENTE], NULL
		mov qword [rax+OFFSET_PALABRA], rbx
		
		pop rbx
		add rsp, 8
		pop rbp
		ret

	; void nodoBorrar( nodo *n );
	nodoBorrar:
		push rbp
		mov rbp, rsp
		sub rsp, 8
		push r12
		
		mov r12, rdi
		
		mov rdi, [r12+OFFSET_PALABRA]
		call free
		
		mov rdi, r12
		call free
		
		pop r12
		add rsp, 8
		pop rbp
		ret

	; lista *oracionCrear( void );
	oracionCrear:
		push rbp
		mov rbp, rsp
		
		mov rdi, LISTA_SIZE
		call malloc
		mov qword [rax+OFFSET_PRIMERO], NULL
		
		pop rbp
		ret

	; void oracionBorrar( lista *l );
	oracionBorrar:
		push rbp
		mov rbp, rsp
		push rbx
		push r12
		
		;rbx = puntero a la palabra que será eliminada
		
		mov r12, rdi
		mov rbx, [rdi+OFFSET_PRIMERO]
		.ciclo:
			cmp rbx, NULL
			je .finCiclo
			mov rdi, rbx
			mov rbx, [rbx+OFFSET_SIGUIENTE]
			call nodoBorrar
			jmp .ciclo
		.finCiclo:
		
		mov rdi, r12
		call free
		
		pop r12
		pop rbx
		pop rbp
		ret

	; void oracionImprimir( lista *l, char *archivo, void (*funcImprimirPalabra)(char*,FILE*) );
	oracionImprimir:
		push rbp
		mov rbp, rsp
		sub rsp, 8
		push rbx
		push r13
		push r12
		
		;rbx = puntero a la palabra que se va imprimir
		;r12 = puntero a la funcion funcImprimirPalabra
		;r13 = puntero al archivo abierto
		
		mov rbx, [rdi+OFFSET_PRIMERO]
		mov r12, rdx
		
		mov rdi, rsi
		mov rsi, MODO_ARCHIVO
		call fopen
		mov r13, rax
		
		cmp rbx, NULL
		je .oracionVacia
		
		.ciclo:
			cmp rbx, NULL
			je .finCiclo
			mov rdi, [rbx+OFFSET_PALABRA]
			mov rsi, r13
			call r12
			mov rbx, [rbx+OFFSET_SIGUIENTE]
			jmp .ciclo
		.finCiclo:
		jmp .fin
		
		.oracionVacia:
			mov rdi, ORACION_VACIA
			mov rsi, r13
			call r12
		
		.fin:
		mov rdi, r13
		call fclose
		
		pop r12
		pop r13
		pop rbx
		add rsp, 8
		pop rbp
		ret

;/** FUNCIONES AVANZADAS **/
;-----------------------------------------------------------

	; float longitudMedia( lista *l );
	longitudMedia:
		push rbp
		mov rbp, rsp
		sub rsp, 8
		push rbx
		push r13
		push r12
		
		;rbx = puntero a la palabra que se esta recorriendo
		;r12 = suma parcial que se va calculando
		;r13 = cantidad de palabras que se van recorriendo
		
		mov rbx, [rdi+OFFSET_PRIMERO]
		mov r13, 0
		mov r12, 0
		.ciclo:
			cmp rbx, NULL
			je .finCiclo
			mov rdi, [rbx+OFFSET_PALABRA]
			call palabraLongitud
			add r12, rax
			inc r13
			mov rbx, [rbx+OFFSET_SIGUIENTE]
			jmp .ciclo
		.finCiclo:
		cmp r13, 0
		jne .fin
		add r13, 1
		.fin:
		cvtsi2ss xmm0, r12
		cvtsi2ss xmm1, r13
		divss xmm0, xmm1
		
		pop r12
		pop r13
		pop rbx
		add rsp, 8
		pop rbp
		ret

	; void insertarOrdenado( lista *l, char *palabra, bool (*funcCompararPalabra)(char*,char*) );
	insertarOrdenado:
		push rbp
		mov rbp, rsp
		sub rsp, 8
		push rbx
		push r13
		push r12
		
		;rbx = puntero a la posición de memoria del campo "siguiente" que
		; contiene la dirección de memoria del nodo que se esta recorriendo (es decir puntero a puntero,
		; , &(l->primero) en caso del primer nodo por ejemplo)
		;
		; r12 = puntero a la funcion funcCompararPalabra
		; r13 = puntero al string palabra
		
		mov r13, rsi
		mov r12, rdx
		lea rbx, [rdi+OFFSET_PRIMERO]
		.ciclo:
			; si estoy al final de la lista (salgo del ciclo)
			cmp qword [rbx], NULL
			je .finCiclo
			; si se cumple la funcion de criterio de corte termina el ciclo
			mov rdi, [rbx]
			mov rdi, [rdi+OFFSET_PALABRA]
			mov rsi, r13
			call r12
			cmp rax, FALSE
			je .finCiclo
			; apunto rbx a la dirección de memoria en donde esta el siguiente elemento
			; es decir a "&((*rbx)->siguiente"
			mov rbx, [rbx]
			lea rbx, [rbx+OFFSET_SIGUIENTE]
			jmp .ciclo
		.finCiclo:
		; creo el nodo nuevo
		mov rdi, r13
		mov rsi, r13
		call nodoCrear
		mov rcx, [rbx]
		; el campo siguiente del nuevo nodo va a ser la direcciòn de memoria en
		; donde está nodo en el que se quedò el loop mov r13, [rbx]
		mov [rax+OFFSET_SIGUIENTE], rcx
		; luego le asigno al nodo en el que me quedé como nodo siguiente el nodo nuevo 
		; (devuelto en rax por malloc) 
		; CASOS BORDES
		; ___________________________________________________________________________________________________
		; CASO PRIMER NODO)
		; Notar que si la lista era vacía o no se cumplio la condición de corte ya en el primer loop  
		; rbx no se modifico y lo que tiene
		; es la direcciòn de memoria del campo lista->primero
		; y por eso cuando hago mov [rbx], rax estoy modificando directamente el campo
		; primero de la lista pasada como paràmetro
		;
		; CASO ULTIMO NODO)
		; Notar que si se llega al ùltimo elemento en rbx tengo la direcciòn de memoria del campo
		; siguiente del ultimo elemento, entonces al hacer mov [rbx], rax estoy modificando
		; ese campo y le asigno la direcciòn de memoria del nuevo nodo
		mov [rbx],rax
		pop r12
		pop r13
		pop rbx
		add rsp, 8
		pop rbp
		ret

	; void eliminar(nodo** nodo);
	; A esta funcion se le pasa la dirección de memoria del campo que contiene
	; la dirección de memoria del puntero que se quiere borrar
	; (&(l->primero) si es el primer nodo el que se quiere eliminar,
	; o la dirección de memoria del campo siguiente del nodo predecesor al que se quiere eliminar).
	; Esta función además arregla los punteros
	eliminar:
		push rbp
		mov rbp, rsp
		
		mov rcx, [rdi]
		mov r10, [rcx+OFFSET_SIGUIENTE]
		mov [rdi], r10
		mov rdi, rcx
		call nodoBorrar
		
		pop rbp
		ret
	
	; void filtrarAltaLista( lista *l, bool (*funcCompararPalabra)(char*,char*), char *palabraCmp );
	filtrarPalabra:
		push rbp
		mov rbp, rsp
		sub rsp, 8
		push rbx
		push r13
		push r12
		
		; rbx = puntero a la posición de memoria del campo "siguiente" que
		; contiene la dirección de memoria del nodo que se esta recorriendo (es decir puntero a puntero,
		; , &(l->primero) en caso del primer nodo por ejemplo)
		; 
		; r12 = puntero al string palabraCmp
		; r13 = puntero a la función funcCompararPalabra

		lea rbx, [rdi+OFFSET_PRIMERO]
		mov r13, rsi
		mov r12, rdx
		
		.ciclo:
			cmp qword [rbx], NULL
			je .finCiclo
			mov rsi, r12
			mov rdi, [rbx]
			mov rdi, [rdi+OFFSET_PALABRA]
			call r13
			cmp al, TRUE
			je .noEliminar
			mov rdi, rbx
			call eliminar
			; al eliminar el ultimo
			; no puedo irme a la direccion de memoria del campo
			; del siguiente nodo porque no hay siguiente nodo
			jmp .ciclo
			.noEliminar:
			mov rbx, [rbx]
			lea rbx, [rbx+OFFSET_SIGUIENTE]
			jmp .ciclo
		.finCiclo:
		
		pop r12
		pop r13
		pop rbx
		add rsp, 8
		pop rbp
		ret
	
	; Funcion que devuelve false
	;------------------------------------
	; bool devolverFalse(void)
	devolverFalse:
		push rbp
		mov rbp, rsp
		mov rax, FALSE
		pop rbp
		ret
	
	; InsertarAdelante
	;-----------------------------------
	; bool insertarAdelanta(lista *l, char *palabra)
	insertarAdelante:
		push rbp
		mov rbp, rsp
		mov rdx, devolverFalse
		call insertarOrdenado
		pop rbp
		ret

	; void descifrarMensajeDiabolico( lista *l, char *archivo, void (*funcImpPbr)(char*,FILE* ) );
	descifrarMensajeDiabolico:
		push rbp
		mov rbp, rsp
		push rbx
		push r12
		push r13
		push r14
		
		;rbx = puntero al nodo que se esta recorriendo
		;r12 = puntero al string archivo
		;r13 = puntero a la función funcImpPbr
		;r14 = puntero a la nueva lista temporal (inversa a l) creada
		
		mov r12, rsi
		mov r13, rdx
		mov rbx, [rdi+OFFSET_PRIMERO]
		cmp rbx, NULL
		je .sinMensajeDiabolico
		
		; creo la misma lista pero insertandoAdelante
		call oracionCrear
		mov r14, rax
		
		.ciclo:
			cmp rbx, NULL
			je .finCiclo
			mov rdi, [rbx+OFFSET_PALABRA]
			call palabraCopiar
			mov rdi, r14
			mov rsi, rax
			call insertarAdelante
			mov rbx, [rbx+OFFSET_SIGUIENTE]
			jmp .ciclo
		.finCiclo:
		
		; imprimir la lista inversa
		mov rdi, r14
		mov rsi, r12 
		mov rdx, r13
		call oracionImprimir
		; y elimino la lista temporal
		mov rdi, r14
		call oracionBorrar
		jmp .fin
		
	.sinMensajeDiabolico:
		mov rdi, r12
		mov rsi, MODO_ARCHIVO
		call fopen
		mov r14, rax
		mov rdi, SIN_MENSAJE_DIABOLICO
		mov rsi, r14
		call r13
		mov rdi, r14
		call fclose
		
	.fin: 
		pop r14
		pop r13
		pop r12
		pop rbx
		pop rbp
		ret
