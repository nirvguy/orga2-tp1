#include "lista.h"
#include <stdio.h>
#include <stdlib.h>

const char* verdad[2] = {"F", "V"};

/** Intercambia minúsculas y mayusculas */
void intercambiarCaracteres(char *p) {
	while(*p != 0) {
		if('a' <= *p && *p <= 'z')
			*(p++) -= 32; 
		else if('A' <= *p && *p <= 'Z')
			*(p++) += 32;
		else
			p++;
	}
}


bool palabraDistinta(char *p1, char *p2) {
	return !palabraIgual(p1,p2);
}

int main (void){
	char palabra[] = "abcDEF012";
	char* palabra_copia;
	printf("Longitud de `hola': %d\n",palabraLongitud("hola"));
	printf("Longitud de `': %d\n",palabraLongitud(""));
	printf("`tata' < `tete': %s\n", verdad[palabraMenor("tata","tete")]);
	printf("`tete' < `tata': %s\n", verdad[palabraMenor("tete","tata")]);
	printf("`cama' < `camada': %s\n", verdad[palabraMenor("cama","camada")]);
	printf("`camada' < `cama': %s\n", verdad[palabraMenor("camada","cama")]);
	printf("`igual' < `igual': %s\n", verdad[palabraMenor("igual","igual")]);
	printf("`cama' < `zorro': %s\n", verdad[palabraMenor("cama","zorro")]);
	printf("`zorro' < `cama': %s\n", verdad[palabraMenor("zorro","cama")]);
	printf("`zorro' < `cama': %s\n", verdad[palabraMenor("zorro","cama")]);
	printf("`cosa' < `casa': %s\n", verdad[palabraMenor("cosa","casa")]);
	printf("`casa' < `coso': %s\n", verdad[palabraMenor("casa","coso")]);
	printf("Minusculas a mayusculas y viceversa de  `%s'",palabra);
	intercambiarCaracteres(palabra);
	printf(": %s\n",palabra);
	printf("Volver a intercambiar `%s` con palabraFormatear",palabra);
	palabraFormatear(palabra,intercambiarCaracteres);
	printf(": %s\n",palabra);
	/* FILE* _stdout=fopen("/dev/stdout","w"); */
	/* printf("Imprimiendo por pantalla %s con palabraImprimir...\n",palabra); */
	/* palabraImprimir(palabra,_stdout); */
	/* fclose(_stdout); */
	/* printf("\n"); */
	palabra_copia = palabraCopiar("esto es una copia");
	printf("Imprimiendo palabra `%s' copiada con palabraCopiar: %s\n","esto es una copia",palabra_copia);
	free(palabra_copia);
	lista* lista_nueva = oracionCrear();
	nodo* hola = nodoCrear(palabraCopiar("hola"));
	nodo* como = nodoCrear(palabraCopiar("como"));
	nodo* estas = nodoCrear(palabraCopiar("estas"));
	hola->siguiente = como;
	como->siguiente = estas;
	lista_nueva->primero = hola;
	printf("La longitud media de la oración es: %f\n", longitudMedia(lista_nueva));
	oracionImprimir(lista_nueva, "/dev/stdout", palabraImprimir); 
	oracionBorrar(lista_nueva);
	lista* lista_abc = oracionCrear();
	insertarOrdenado(lista_abc,palabraCopiar("c"),palabraMenor);
	insertarOrdenado(lista_abc,palabraCopiar("e"),palabraMenor);
	insertarOrdenado(lista_abc,palabraCopiar("d"),palabraMenor);
	insertarOrdenado(lista_abc,palabraCopiar("a"),palabraMenor);
	insertarOrdenado(lista_abc,palabraCopiar("a"),palabraMenor);
	insertarOrdenado(lista_abc,palabraCopiar("b"),palabraMenor);
	oracionImprimir(lista_abc, "/dev/stdout", palabraImprimir); 
	printf("\n");
	filtrarPalabra(lista_abc, palabraIgual, "a"); 
	filtrarPalabra(lista_abc, palabraIgual, "c"); 
	filtrarPalabra(lista_abc, palabraIgual, "d"); 
	/* filtrarPalabra(lista_abc, palabraIgual, "e"); */
	oracionImprimir(lista_abc, "/dev/stdout", palabraImprimir);  
	printf("\n");
	oracionBorrar(lista_abc);
	lista* lista_diabolica = oracionCrear();
	insertarAtras(lista_diabolica, palabraCopiar("bien?"));
	insertarAtras(lista_diabolica, palabraCopiar("todo"));
	insertarAtras(lista_diabolica, palabraCopiar("estas"));
	insertarAtras(lista_diabolica, palabraCopiar("como"));
	insertarAtras(lista_diabolica, palabraCopiar("hola"));
	oracionImprimir(lista_diabolica, "/dev/stdout", palabraImprimir);
	printf("\n");
	descifrarMensajeDiabolico(lista_diabolica,"/dev/stdout",palabraImprimir);
	printf("\n");
	oracionBorrar(lista_diabolica);
	lista* lista_vacia = oracionCrear();
	oracionImprimir(lista_vacia, "/dev/stdout", palabraImprimir);
	descifrarMensajeDiabolico(lista_vacia,"/dev/stdout",palabraImprimir);
	oracionBorrar(lista_vacia);
	lista* lista_palabras_ordenadas = oracionCrear();
	insertarOrdenado(lista_palabras_ordenadas, palabraCopiar("abaco"), palabraMenor);
	printf("palabraLongitud de %s, %d\n", "abaco", palabraLongitud("abaco"));
	insertarOrdenado(lista_palabras_ordenadas, palabraCopiar("arbol"), palabraMenor);
	printf("palabraLongitud de %s, %d\n", "arbol", palabraLongitud("arbol"));
	insertarOrdenado(lista_palabras_ordenadas, palabraCopiar("arboleda"), palabraMenor);
	printf("palabraLongitud de %s, %d\n", "arboleda", palabraLongitud("arboleda"));
	insertarOrdenado(lista_palabras_ordenadas, palabraCopiar("mesa"), palabraMenor);
	printf("palabraLongitud de %s, %d\n", "mesa", palabraLongitud("mesa"));
	insertarOrdenado(lista_palabras_ordenadas, palabraCopiar("tabla"), palabraMenor);
	printf("palabraLongitud de %s, %d\n", "tabla", palabraLongitud("tabla"));
	insertarOrdenado(lista_palabras_ordenadas, palabraCopiar("mazon"), palabraMenor);
	printf("palabraLongitud de %s, %d\n", "mazon", palabraLongitud("mazon"));
	insertarOrdenado(lista_palabras_ordenadas, palabraCopiar("reza"), palabraMenor);
	printf("palabraLongitud de %s, %d\n", "reza", palabraLongitud("reza"));
	insertarOrdenado(lista_palabras_ordenadas, palabraCopiar("raza"), palabraMenor);
	printf("palabraLongitud de %s, %d\n", "raza", palabraLongitud("raza"));
	insertarOrdenado(lista_palabras_ordenadas, palabraCopiar("zilofono"), palabraMenor);
	printf("palabraLongitud de %s, %d\n", "zilofono", palabraLongitud("zilofono"));
	oracionImprimir(lista_palabras_ordenadas, "/dev/stdout", palabraImprimir);
	oracionBorrar(lista_palabras_ordenadas);
	printf("\n");
	lista* oracion_ordenada = oracionCrear();
	insertarOrdenado(oracion_ordenada, palabraCopiar("pato"), palabraMenor);
	insertarOrdenado(oracion_ordenada, palabraCopiar("pico"), palabraMenor);
	insertarOrdenado(oracion_ordenada, palabraCopiar("casa"), palabraMenor);
	insertarOrdenado(oracion_ordenada, palabraCopiar("casa"), palabraMenor);
	insertarOrdenado(oracion_ordenada, palabraCopiar("pasta"), palabraMenor);
	insertarOrdenado(oracion_ordenada, palabraCopiar("carcaza"), palabraMenor);
	insertarOrdenado(oracion_ordenada, palabraCopiar("carcaza"), palabraMenor);
	insertarOrdenado(oracion_ordenada, palabraCopiar("carcaza"), palabraMenor);
	insertarOrdenado(oracion_ordenada, palabraCopiar("carcaza"), palabraMenor);
	insertarOrdenado(oracion_ordenada, palabraCopiar("a"), palabraMenor);
	insertarOrdenado(oracion_ordenada, palabraCopiar("a"), palabraMenor);
	oracionImprimir(oracion_ordenada, "/dev/stdout", palabraImprimir);
	printf("\n");
	filtrarPalabra(oracion_ordenada, palabraDistinta, "carcaza");
	oracionImprimir(oracion_ordenada, "/dev/stdout", palabraImprimir);
	oracionBorrar(oracion_ordenada);
	return 0;
}
